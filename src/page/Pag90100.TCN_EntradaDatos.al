page 90100 "TCN_EntradaDatos"
{
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    Caption = 'Entrada de datos';

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                field(Fecha1; ArrayFechas[1])
                {
                    ApplicationArea = All;
                    Visible = xVisibleFecha1;
                    CaptionClass = CaptionF(ArrayFechas[1], 1);
                }
                field(Fecha2; ArrayFechas[2])
                {
                    ApplicationArea = All;
                    Visible = xVisibleFecha1;
                    CaptionClass = CaptionF(ArrayFechas[2], 2);
                }
                field(Fecha3; ArrayFechas[3])
                {
                    ApplicationArea = All;
                    Visible = xVisibleFecha1;
                    CaptionClass = CaptionF(ArrayFechas[3], 3);
                }
                field(Numero1; ArrayNumeros[1])
                {
                    ApplicationArea = All;
                    Visible = xVisibleNum1;
                    CaptionClass = CaptionF(ArrayNumeros[1], 1);
                }
                field(Numero2; ArrayNumeros[2])
                {
                    ApplicationArea = All;
                    Visible = xVisibleNum2;
                    CaptionClass = CaptionF(ArrayNumeros[2], 2);
                }
                field(Texto1; ArrayTextos[1])
                {
                    ApplicationArea = All;
                    Visible = xVisibleText1;
                    CaptionClass = CaptionF(ArrayTextos[1], 1);
                }
                field(Texto2; ArrayTextos[2])
                {
                    ApplicationArea = All;
                    Visible = xVisibleText2;
                    CaptionClass = CaptionF(ArrayTextos[2], 2);
                }
            }
        }
    }

    var
        // Variables Visibles
        xVisibleFecha1: Boolean;
        xVisibleFecha2: Boolean;
        xVisibleFecha3: Boolean;
        xVisibleNum1: Boolean;
        xVisibleNum2: Boolean;
        xVisibleText1: Boolean;
        xVisibleText2: Boolean;

        // Arrays
        ArrayTextos: array[2] of Text;
        ArrayNumeros: array[2] of Decimal;
        ArrayFechas: array[3] of Date;

        // Variables funcion CaptionF
        xIdFecha: Integer;
        xIdNumero: Integer;
        xIdText: Integer;
        ArrayCaptions: array[3, 3] of Text;

        // Punteros Set y Get
        ArraySet: array[3] of Integer;
        ArrayGet: array[3] of Integer;



        // Funcion para los campos
    local procedure CaptionF(pTipoVariable: Variant; pNumCampo: Integer) xSalida: Text
    var
        xlTipoDato: Integer;
    begin
        xSalida := '3,';
        if pTipoVariable.IsDate then begin
            xlTipoDato := xIdFecha;
        end else
            if pTipoVariable.IsDecimal then begin
                xlTipoDato := xIdNumero;
            end else
                if pTipoVariable.IsText then begin
                    xlTipoDato := xIdText;
                end else begin
                    Error('Tipo de dato desconocido');
                end;

        if xlTipoDato in [1 .. 3] then begin
            xSalida += ArrayCaptions[xlTipoDato, pNumCampo];
        end;

    end;
    // Funcion SET
    procedure SetDatosF(pTituloCampo: Text; pCampo: date)
    var
        xlId: Integer;
    begin
        xlId := xIdFecha;
        ArraySet[xlId] += 1;
        if ArraySet[xlId] > ArrayLen(ArrayFechas) then begin
            Error('Número de Fecha Superior');
        end;
        case ArraySet[xlId] of
            1:
                xVisibleFecha1 := true;
            2:
                xVisibleFecha2 := true;
        end;
        ArrayCaptions[xlId, ArraySet[xlId]] := pTituloCampo;
        ArrayFechas[ArraySet[xlId]] := pCampo;
    end;

    procedure SetDatosF(pTituloCampo: Text; pCampo: Decimal)
    var
        xlId: Integer;
    begin

    end;
}